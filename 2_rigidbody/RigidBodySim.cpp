#include "RigidBodySim.h"

/////////////////////////////////////
///////////// EX 1 //////////////////
/////////////////////////////////////
bool RigidBodySim::advance() {
    // compute the collision detection
    m_collisionDetection.computeCollisionDetection(m_broadPhaseMethod, m_narrowPhaseMethod, m_eps);

    // apply forces (only gravity in this case)
    for (auto &o : m_objects) {
        o.applyForceToCOM(m_gravity);
    }

    for (auto &o : m_objects) {
        /////
        // TODO1: integrate velocities

        /////

        o.resetForce();
        o.resetTorque();

        /////
        // TODO2: integrate position

        /////

        /////
        // TODO3: integrate orientation
        // get the angular velocity
        Eigen::Vector3d w = o.getAngularVelocity();

        switch (m_method) {
        case 0: {
            // matrix-based angular velocity
            Eigen::Matrix3d W;
            Eigen::Matrix3d r = o.getRotationMatrix();

            // get a skew-matrix W of w

            
            // update rotation matrix r


            // orthogonalize rotation matrix
            // https://math.stackexchange.com/questions/3292034/normalizing-a-rotation-matrix
            // https://en.wikipedia.org/wiki/Orthogonal_Procrustes_problem
            // idea is to find the nearest orthogonral matrix by SVD
            Eigen::JacobiSVD<Eigen::Matrix3d> svd(r, Eigen::ComputeFullU | Eigen::ComputeFullV);
            r = svd.matrixU() * svd.matrixV().transpose();

            o.setRotation(r);
            break;
        }
        default: {
            // quaternion-based
            Eigen::Quaterniond wq;
            wq.w() = 0;
            wq.vec() = w;

            Eigen::Quaterniond q = o.getRotation();
            Eigen::Quaterniond dq = wq * q;
            Eigen::Quaterniond new_q;
            new_q.w() = q.w() + 0.5 * m_dt * dq.w();
            new_q.vec() = q.vec() + 0.5 * m_dt * dq.vec();
            o.setRotation(new_q.normalized());
            break;
        }
        }
    }

    // advance time
    m_time += m_dt;
    m_step++;

    return false;
}