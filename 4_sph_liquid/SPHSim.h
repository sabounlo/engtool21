#include <igl/colormap.h>
#include <igl/edges.h>
#include "Simulation.h"
#include "ParticlesSampler.h"
#include "Kernel.h"
#include "Array2T.h"

using namespace std;

/*
 * SPH Simulation of a dam breaking.
 */
class SPHSim : public Simulation {
public:
	SPHSim() : Simulation() { init(); }

	virtual void init() override {
		// set constants
		m_k = 50.0;
		m_exp = 7;
		m_rest = 1000.0;
		m_mu = 0.002;
		m_gravity = Eigen::Vector3d(0, -9.81, 0);
		m_r = 0.025;
		m_h = 4 * m_r;

		m_cellSize = 2 * m_r; // 1 particle per cell
		m_gridResolution = Eigen::Vector3i(128, 64, 1); // 2d if res_z is 1
		//m_gridResolution = Eigen::Vector3i(32, 16, 1); // 2d if res_z is 1
		m_dt = 0.05; // max dt
		m_cflFactor = 0.05;
		m_dt_ = m_dt;
		m_e = 0.5;

		double particleVolume = 0.8 * (2 * m_r) * (2 * m_r);
		if (m_gridResolution.z() > 1) {
			particleVolume *= (2 * m_r);
		}
		m_mass = particleVolume * m_rest;

		// domain setup
		m_gridStart.setZero();
		m_gridEnd = Eigen::Vector3d(m_gridResolution.x() * m_cellSize, m_gridResolution.y() * m_cellSize, m_gridResolution.z() * m_cellSize);

		// domain visualization
		m_pDomain = new Grid2(m_gridResolution.x(), m_gridResolution.y(), m_cellSize);
		m_pDomain->getMesh(m_renderV, m_renderF); // need to call once
		m_pDomain->getColors(m_renderC);

		m_cellSizeFactor = 2; // relative cell size of grid for neighbor search
		
		// color field for visualization
		m_field = 0; // id

		// scene setup: dambreak
		Eigen::Vector3i damResolution(int(m_gridResolution.x() * 0.35 + 0.5), int(m_gridResolution.y() * 0.6 + 0.5), m_gridResolution.z());
		ParticlesSampler particleSampler(damResolution, m_cellSize);
		m_pParticleData = particleSampler.getParticlesData();

		int n = m_pParticleData->getNumParticles();
		cout << "# particles: " << n << endl;

		// add attributes
		vector<float> density(n, m_rest);
		OwnCustomAttribute<vector<float>>::get(m_pParticleData)->setAttribute("density", density);
		//m_pParticleData->setAttribute<vector<float>>("density", density);

		vector<float> pressure(n, 0);
		OwnCustomAttribute<vector<float>>::get(m_pParticleData)->setAttribute("pressure", pressure);

		vector<Eigen::Vector3d> force(n, Eigen::Vector3d::Zero());
		OwnCustomAttribute<vector<Eigen::Vector3d>>::get(m_pParticleData)->setAttribute("force", force);

		// for particle rendering
		m_renderP = Eigen::MatrixXd::Zero(n, 3);
		m_renderPC = Eigen::MatrixXd::Zero(n, 3);

		// set kernel functions
		Kernel::setRadius(m_h);
		m_kernelFct = Kernel::W_cubic;
		m_gradKernelFct = Kernel::gradW_cubic;
		m_lapKernelFct = Kernel::lapW_visco;

		// initialize for visualization
		findNeighbors();
		computeDensity();

		reset();
	}

	virtual void resetMembers() override {
		Eigen::Vector3i damResolution(int(m_gridResolution.x() * 0.35 + 0.5), int(m_gridResolution.y() * 0.6 + 0.5), m_gridResolution.z());

		int n = m_pParticleData->getNumParticles();
		vector<Eigen::Vector3d>& position = m_pParticleData->getPositions();
		vector<Eigen::Vector3d>& velocity = m_pParticleData->getVelocities();

		uint pid = 0;
		for (uint i = 0; i < damResolution.x(); i++) {
			for (uint j = 0; j < damResolution.y(); j++) {
				for (uint k = 0; k < damResolution.z(); k++) {
					Eigen::Vector3d particlePosition = Eigen::Vector3d(i + 0.5, j + 0.5, k + 0.5);
					position[pid] = particlePosition * m_cellSize;
					velocity[pid] = Eigen::Vector3d::Zero();
					pid++;
				}
			}
		}
	}

	virtual void updateRenderGeometry() override {
		int n = m_pParticleData->getNumParticles();
		auto p = m_pParticleData->getPositions();
		for (int i = 0; i < n; ++i) {
			m_renderP.row(i) = p[i];
		}

		if (m_field == 0) {
			for (int i = 0; i < n; ++i) {
				double r, g, b;
				igl::colormap(igl::COLOR_MAP_TYPE_VIRIDIS, (double)i / (n - 1), r, g, b);
				m_renderPC.row(i) = Eigen::Vector3d(r, g, b);
			}
		}
		else if (m_field == 1) {
			auto density = OwnCustomAttribute<vector<float>>::get(m_pParticleData)->getAttribute("density");
			float d_min = density[0];
			float d_max = density[0];
			for (int i = 0; i < n; ++i) {
				d_min = min(d_min, density[i]);
				d_max = max(d_max, density[i]);
			}

			float d_range = d_max - d_min;
			if (d_range < 1e-6) {
				m_renderPC.setOnes();
			}
			else {
				for (int i = 0; i < n; ++i) {
					double r, g, b;
					double d = (density[i] - d_min) / d_range;
					igl::colormap(igl::COLOR_MAP_TYPE_VIRIDIS, d, r, g, b);
					m_renderPC.row(i) = Eigen::Vector3d(r, g, b);
				}
			}
		}
		else {
			auto velocity = m_pParticleData->getVelocities();
			double v_max = (m_gridEnd - m_gridStart).norm();
			for (int i = 0; i < n; ++i) {
				double v_len = velocity[i].norm();
				double v = v_len / v_max;
				double r = (1 - v) * 0.26 + v;
				double g = (1 - v) * 0.5 + v;
				double b = (1 - v) * 0.73 + v;
				m_renderPC.row(i) = Eigen::Vector3d(r, g, b);
			}
		}
	}

	virtual bool advance() override {
		findNeighbors();
		computeDensity();
		computePressure();
		computeForce();
		advectParticles();
		collisionHandling();
		updateDt();

		// advance m_time
		m_time += m_dt_;
		m_step++;

		return false;
	}

	virtual void renderRenderGeometry(
		igl::opengl::glfw::Viewer& viewer) override {
		viewer.data().set_mesh(m_renderV, m_renderF);
		viewer.data().set_colors(m_renderC);
		viewer.data().add_points(m_renderP, m_renderPC);
	}	
#pragma region FluidSteps
	Eigen::Vector2i getGridIndex(const Eigen::Vector3d& pos) {
		int x = min(max(int(pos.x() / (m_cellSize * m_cellSizeFactor)), 0), m_grid.size(0) - 1);
		int y = min(max(int(pos.y() / (m_cellSize * m_cellSizeFactor)), 0), m_grid.size(1) - 1);
		return Eigen::Vector2i(x, y);
	}

	void findNeighbors() {
		m_grid = NeighborArray2d(m_gridResolution.x()/m_cellSizeFactor, m_gridResolution.y()/m_cellSizeFactor, vector<int>());
		int n = m_pParticleData->getNumParticles();
		auto position = m_pParticleData->getPositions();
		
		for (int i = 0; i < n; ++i) {
			const Eigen::Vector2i& idx = getGridIndex(position[i]);
			m_grid(idx.x(), idx.y()).push_back(i);
		}
	}

	vector<int> getNeighbors(const Eigen::Vector3d& pos) {
		const Eigen::Vector2i& idx = getGridIndex(pos);
		int x0 = max(idx.x()-1, 0);
		int x1 = min(idx.x()+1, m_grid.size(0)-1);
		int y0 = max(idx.y()-1, 0);
		int y1 = min(idx.y()+1, m_grid.size(1)-1);
		vector<int> neighbors;
		for (int i = x0; i <= x1; ++i) {
			for (int j = y0; j <= y1; ++j) {
				if (m_grid(i, j).size() > 0)
					neighbors.insert(neighbors.end(), m_grid(i, j).begin(), m_grid(i, j).end());
			}
		}
		return neighbors;
	}

	void advectParticles() {
		int n = m_pParticleData->getNumParticles();
		auto force = OwnCustomAttribute<vector<Eigen::Vector3d>>::get(m_pParticleData)->getAttribute("force");
		vector<Eigen::Vector3d>& velocity = m_pParticleData->getVelocities();
		vector<Eigen::Vector3d>& position = m_pParticleData->getPositions();
		
		#pragma omp parallel for
		for (int i = 0; i < n; ++i) {
			velocity[i] += m_dt_ * force[i] / m_mass;
			position[i] += velocity[i] * m_dt_;
		}
	}

	void collisionHandling() {
		int n = m_pParticleData->getNumParticles();
		vector<Eigen::Vector3d>& position = m_pParticleData->getPositions();
		vector<Eigen::Vector3d>& velocity = m_pParticleData->getVelocities();
		double eps = 0.05 * m_r;

		#pragma omp parallel for
		for (int i = 0; i < n; ++i) {
			if (position[i].x() < m_gridStart.x()) {
				position[i](0) = m_gridStart.x() + eps;
				velocity[i](0) = m_e * -velocity[i].x();
			}
			if (position[i].x() > m_gridEnd.x()) {
				position[i](0) = m_gridEnd.x() - eps;
				velocity[i](0) = m_e * -velocity[i].x();
			}
			if (position[i].y() < m_gridStart.y()) {
				position[i](1) = m_gridStart.y() + eps;
				velocity[i](1) = m_e * -velocity[i].y();
			}
			if (position[i].y() > m_gridEnd.y()) {
				position[i](1) = m_gridEnd.y() - eps;
				velocity[i](1) = m_e * -velocity[i].y();
			}
			if (position[i].z() < m_gridStart.z()) {
				position[i](2) = m_gridStart.z() + eps;
				velocity[i](2) = m_e * -velocity[i].z();
			}
			if (position[i].z() > m_gridEnd.z()) {
				position[i](2) = m_gridEnd.z() - eps;
				velocity[i](2) = m_e * -velocity[i].z();
			}
		}
	}

	void updateDt() {
		double maxVel = 0;
		for (auto v : m_pParticleData->getVelocities()) {
			maxVel = max(maxVel, v.norm());
		}
		m_dt_ = m_cflFactor * 0.4 * (m_r * 2 / (sqrt(maxVel)));
		m_dt_ = min(m_dt_, m_dt); 
	}

	float W(const Eigen::Vector3d& r) const { return m_kernelFct(r); }
	Eigen::Vector3d gradW(const Eigen::Vector3d& r) { return m_gradKernelFct(r); }
	float lapW(const Eigen::Vector3d& r) { return m_lapKernelFct(r); }

#pragma endregion FluidSteps

#pragma region Exercise
	void computeDensity();
	void computePressure();
	void computeForce();
#pragma endregion Exercise

#pragma region SettersAndGetters
	void selectField(int field) { m_field = field; }
	void setK(double k) { m_k = k; }
	void setMu(double mu) { m_mu = mu; }

	shared_ptr<ParticlesData> getParticlesData() {
		return m_pParticleData;
	}
	int getField() const { return m_field; }
	double getK() const { return m_k; }
	double getMu() const { return m_mu; }
	double getTimestep() const { return m_dt; }
#pragma endregion SettersAndGetters

private:
	Eigen::Vector3d m_gridStart;
	Eigen::Vector3d m_gridEnd;
	Eigen::Vector3i m_gridResolution;
	double m_cellSize;

	Eigen::Vector3d m_gravity;
	double m_k;	// stiffness
	double m_exp; // exponent
	double m_rest;	// rest density
	double m_r;	// radius
	double m_h;	// support radius
	double m_mu;	// viscosity
	double m_mass;	// mass
	double m_cflFactor; // cfl factor
	double m_dt_; // dt after applying cfl condition
	double m_e; // coefficient of restitution

	float(*m_kernelFct)(const Eigen::Vector3d&);
	Eigen::Vector3d(*m_gradKernelFct)(const Eigen::Vector3d&);
	float(*m_lapKernelFct)(const Eigen::Vector3d&);

	shared_ptr<ParticlesData> m_pParticleData;
	Grid2* m_pDomain;
	NeighborArray2d m_grid; // for neighbor search
	int m_cellSizeFactor;
	int m_field;

	Eigen::MatrixXd m_renderV; // vertex positions, 
	Eigen::MatrixXi m_renderF; // face indices 
	Eigen::MatrixXd m_renderC; // face (or vertex) colors for rendering

	Eigen::MatrixXd m_renderP; // particle positions
	Eigen::MatrixXd m_renderPC; // particle colors
};